import { Injectable } from '@angular/core';
import { observable, action } from 'mobx';

import { Object } from '../modules/models/object.model';
import { ObjectService } from '../modules/services/object/object.service';

@Injectable({
    providedIn: 'root'
})
export class ObjectStore {
    constructor(private objectService: ObjectService){}

    @observable objects: Object[] = [];
    @observable errorMessage = '';
    @observable storageSizeInBytes = 0
    @observable storageSizeInUnits = '';
    units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    @action addObject(formData: FormData, bucketId: string) {
        this.objectService.uploadObject(formData, bucketId).subscribe(data => {
            this.getObjects(bucketId);
        })
    }

    @action getObjects(id: string) {
        this.objectService.getAllObjects(id).subscribe(data => {
            this.objects = data["objects"];
            this.calculateStorageSize(data);
        })
    }

    @action deleteObject(bucketId: string, objectId: string) {
        this.objectService.deleteObject(bucketId, objectId).subscribe(data => {
            this.getObjects(bucketId);
        })
    }

    @action setErrorMessage(error: string) {
        this.errorMessage = error;
    }

    convertBytes(x){
        let l = 0, n = parseInt(x, 10) || 0;
        while(n >= 1024 && ++l)
            n = n/1024;

        return(n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + this.units[l]);
    }

    calculateStorageSize(data) {
        this.storageSizeInBytes = 0;
        data["objects"].forEach(element => {
            this.storageSizeInBytes += element.size;
        });

        this.storageSizeInUnits = this.convertBytes(this.storageSizeInBytes);
    }
}
