import { Injectable } from '@angular/core';
import { observable, action } from 'mobx';

import { Bucket } from '../modules/models/bucket.model';
import { BucketService } from '../modules/services/bucket/bucket.service';

@Injectable({
    providedIn: 'root'
})

export class BucketStore {
    constructor(private bucketService: BucketService){}

    @observable buckets: Bucket[] = [];
    @observable bucketAddEnabled: boolean = false;


    @action addBucket(bucket: Bucket) {
        this.buckets.push(bucket);

        const finalData: Object = {
            name: bucket.name,
            location: bucket.location.id
          }

        this.bucketService.addBucket(finalData).subscribe(response => {
        });
    }

    @action setBucketAddStatus(status: boolean){
        this.bucketAddEnabled = status;
    }

    @action getBucket(id: number) {
        return this.buckets[id];
    }

}
