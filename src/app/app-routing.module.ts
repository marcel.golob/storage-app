import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BucketsComponent } from './modules/components/buckets/buckets.component';
import { BucketDetailsComponent } from './modules/components/buckets/bucket-details/bucket-details.component';


const routes: Routes = [
  { path: '', component: BucketsComponent },
  { path: 'buckets/:id', component: BucketDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
