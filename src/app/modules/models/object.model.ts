import { Bucket } from './bucket.model';

export class Object {
    name?: string;
    modified?: string;
    size?: number;
    bucket: Bucket;
    file?: string;
}