import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Location } from '../../models/location.model';
import { HttpClientService } from '../http-client.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  
  constructor(
    private http: HttpClient,
    private httpClientService: HttpClientService
  ) { }

  getLocations() {
    return this.http.get<Location[]>(
      this.httpClientService.getApiServer() + '/locations', 
      this.httpClientService.getAuthToken()
    );
  }
}
