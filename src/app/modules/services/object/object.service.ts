import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HttpClientService } from '../http-client.service';
import { Object } from '../../models/object.model';

@Injectable({
  providedIn: 'root'
})
export class ObjectService {

  constructor(private httpClientService: HttpClientService, private http: HttpClient) { }

  uploadObject(formData: FormData, bucketId: string) {
    return this.http
    .post(
      this.httpClientService.getApiServer() + '/buckets/' + bucketId + '/objects', 
      formData,
      this.httpClientService.getAuthToken()
    );
  }

  getAllObjects(bucketId: string){
    return this.http
    .get<Object[]>(
      this.httpClientService.getApiServer() + '/buckets/' + bucketId +'/objects', 
      this.httpClientService.getAuthToken()
    );
  }

  deleteObject(bucketId: string, objectId: string) {
    return this.http
    .delete<Object>(
      this.httpClientService.getApiServer() + '/buckets/' + bucketId + '/objects/' + objectId, 
      this.httpClientService.getAuthToken()
    );
  }
}
