import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Bucket } from 'src/app/modules/models/bucket.model';
import { HttpClientService } from '../http-client.service';

@Injectable({
  providedIn: 'root'
})
export class BucketService {
  //private buckets: Bucket[] = [];

  constructor(
    private http: HttpClient,
    private httpClientService: HttpClientService,
  ){}

  getBuckets() {
    return this.http
      .get<Bucket[]>(this.httpClientService.getApiServer() + '/buckets', 
      this.httpClientService.getAuthToken());
  }

  addBucket(bucketData: any) {
    return this.http
    .post<Bucket>(
      this.httpClientService.getApiServer() + '/buckets', 
      bucketData,
      this.httpClientService.getAuthToken()
    );
  }

  getBucket(id: number) {
    return this.http
      .get<Bucket>(this.httpClientService.getApiServer() + '/buckets/' + id, 
      this.httpClientService.getAuthToken());
  }

  deleteBucket(id: string) {
    return this.http
    .delete(
      this.httpClientService.getApiServer() + '/buckets/' + id, 
      this.httpClientService.getAuthToken()
    );
  }
}
