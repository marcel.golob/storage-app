import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor() { }

  private apiServer = 'https://challenge.3fs.si/storage';

  httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'Token 5CD8002A-82FD-4691-A258-8CAB25B0B4CA',
    })
  }

  getApiServer() {
    return this.apiServer;
  }

  getAuthToken() {
    return this.httpOptions;
  }
}
