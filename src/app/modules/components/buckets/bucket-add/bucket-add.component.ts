import { action } from 'mobx-angular';
import { 
  Component, 
  OnInit 
} from '@angular/core';
import { 
  FormGroup, 
  FormControl, 
  Validators 
} from '@angular/forms';


import { LocationService } from 'src/app/modules/services/location/location.service';
import { BucketStore } from 'src/app/stores/bucket-store.store';
import { Bucket } from 'src/app/modules/models/bucket.model';
import { Location } from 'src/app/modules/models/location.model';

@Component({
  selector: 'app-bucket-add',
  templateUrl: './bucket-add.component.html',
  styleUrls: ['./bucket-add.component.scss']
})
export class BucketAddComponent implements OnInit {
  formSubmitted = false;
  locations: Location[] = [];
  selectedLocation: string = '';

  bucketGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    location: new FormControl('', [Validators.required])
  });

  get name(): any {
    return this.bucketGroup.get('name');
  }

  get location(): any {
    return this.bucketGroup.get('location');
  }

  constructor(
    private locationService: LocationService,
    public store: BucketStore
  ) {}

  onFetchLocations() {
    this.locationService.getLocations().subscribe(data => {
      this.locations = data["locations"];
    })
  }

  @action.bound onAddBucket() {
    this.formSubmitted = true;
    const request: Bucket = new Bucket();

    request.location = {
      id: '',
      name: ''
    }

    request.name = this.bucketGroup.get('name').value;
    request.location.id = this.bucketGroup.get('location').value;
    request.location.name = this.selectedLocation;

    const finalData: Object = {
      name: request.name,
      location: request.location
    }

    if (this.bucketGroup.valid) {
      this.store.addBucket(request);
      this.store.setBucketAddStatus(false);
    }
  }

  onSelectedLocation($event) {
    this.selectedLocation = $event.target.options[$event.target.options.selectedIndex].text;
  }

  ngOnInit(): void {
    this.onFetchLocations() 
  }
}
