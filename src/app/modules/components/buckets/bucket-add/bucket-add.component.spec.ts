import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BucketAddComponent } from './bucket-add.component';
import { Bucket } from 'src/app/modules/models/bucket.model';

describe('BucketAddComponent', () => {
  let component: BucketAddComponent;
  let fixture: ComponentFixture<BucketAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BucketAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BucketAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.bucketGroup.valid).toBeFalsy();
  });

  it('name field validity', () => {
    let name = component.bucketGroup.controls['name'];
    expect(name.valid).toBeFalsy();
  });

  it('location field validity', () => {
    let location = component.bucketGroup.controls['location'];
    expect(location.valid).toBeFalsy();
  });

  it('should fetch locations', () => {
    component.onFetchLocations();
    expect(component.onFetchLocations() != null);
  });

  it('submitting a form to add a bucket', () => {
    expect(component.bucketGroup.valid).toBeFalsy();
    component.bucketGroup.controls['name'].setValue("Test bucket 123");
    component.bucketGroup.controls['location'].setValue("571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4");
    expect(component.bucketGroup.valid).toBeTruthy();

    let bucket: Bucket;

    expect(bucket.name).toBe("Test bucket 123");
    expect(bucket.location.id).toBe("571E30F3-7A96-45FF-8FDE-0A3F0E6BBDF4");
  });
});
