import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { action } from 'mobx-angular';
import { 
  NgbModal, 
  ModalDismissReasons, 
  NgbModalRef 
} from '@ng-bootstrap/ng-bootstrap';

import { Bucket } from 'src/app/modules/models/bucket.model';
import { BucketService } from 'src/app/modules/services/bucket/bucket.service';
import { BucketStore } from 'src/app/stores/bucket-store.store';
import { ObjectStore } from 'src/app/stores/object-store.store';


@Component({
  selector: 'app-bucket-details',
  templateUrl: './bucket-details.component.html',
  styleUrls: ['./bucket-details.component.scss']
})
export class BucketDetailsComponent implements OnInit {

  constructor(
    private router: Router, 
    private route: ActivatedRoute, 
    private bucketService: BucketService, 
    private bucketStore: BucketStore,
    public objectStore: ObjectStore,
    private modalService: NgbModal
  ) { }

  objects: Object[] = []
  bucket: Bucket;
  id: number;
  closeResult: string;
  modalReference: NgbModalRef;
  storageSize: number;

  @action.bound getBucket(id){
    return this.bucketStore.buckets[this.id];
  }

  ngOnInit(): void {
    this.route.params
      .subscribe((params: Params) => {
        this.id = +params['id'];
        this.bucket = this.getBucket(this.id);
    });
  }

  onDeleteBucket() {
    this.bucketService.deleteBucket(this.bucket.id).subscribe(response => {
      this.modalReference.close();
      this.router.navigate([''], {relativeTo: this.route});
    });
  }
    
  openConfirmModal(content) {
    this.modalReference = this.modalService.open(content);
    this.modalReference.componentInstance;
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
