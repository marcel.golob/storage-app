import { Component, OnInit } from '@angular/core';
import { action } from 'mobx-angular';

import { Bucket } from '../../models/bucket.model';
import { Location } from '../../models/location.model';
import { BucketService } from 'src/app/modules/services/bucket/bucket.service';
import { BucketStore } from 'src/app/stores/bucket-store.store';


@Component({
  selector: 'app-buckets',
  templateUrl: './buckets.component.html',
  styleUrls: ['./buckets.component.scss']
})
export class BucketsComponent implements OnInit {
  selectedBucket: string;
  _buckets: Bucket[] = [];
  locations: Location[] = [];

  constructor(
    private bucketService: BucketService, 
    public store: BucketStore,
  ) {}

  ngOnInit(): void {
    this.onFetchBuckets(); 
    this._buckets = this.store.buckets;
  }

  @action.bound onCreateNewButtonClick() {
    this.store.setBucketAddStatus(true);
  }

  onFetchBuckets() {
    this.bucketService.getBuckets().subscribe(data => {
      this.store.buckets = data["buckets"];
    })
  }

  onSelectedItem(id: string) {
    this.selectedBucket = id;
  }
}
