import { 
  Component, 
  OnInit
} from '@angular/core';
import { LocationService } from 'src/app/modules/services/location/location.service';
import { Location } from '../../models/location.model';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  locations: Location[] = [];

  constructor(private locationService: LocationService) { }

  ngOnInit(): void {
    this.fetchLocations();
  }

  fetchLocations() {
    this.locationService.getLocations().subscribe(data => {
      this.locations = data["locations"];
    })
  }

}
