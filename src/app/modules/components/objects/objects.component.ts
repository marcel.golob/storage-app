import { action } from 'mobx-angular';
import { ActivatedRoute } from '@angular/router';
import { 
  Component, 
  OnInit, 
  Input 
} from '@angular/core';
import { 
  FormGroup, 
  Validators, 
  FormControl 
} from '@angular/forms';

import { ObjectStore } from 'src/app/stores/object-store.store';
import { Object } from '../../models/object.model';
import { Bucket } from '../../models/bucket.model';

import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-objects',
  templateUrl: './objects.component.html',
  styleUrls: ['./objects.component.scss']
})
export class ObjectsComponent implements OnInit {
  objects: Object[] = [];
  bucketId: number;
  object: Object;
  modalReference: NgbModalRef;
  closeResult: string;
  @Input() bucket: Bucket; 
  selected: Object;

  fileFormGroup = new FormGroup({
    bucketId: new FormControl('', [Validators.required]),
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required])
  });

  constructor(
    private store: ObjectStore,
    private route: ActivatedRoute, 
    public objectStore: ObjectStore,
    private modalService: NgbModal
  ) { }

  get formControls(){
    return this.fileFormGroup.controls;
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.fileFormGroup.patchValue({
        fileSource: file
      });

      this.uploadObject();
    }
  }

  @action.bound uploadObject() {
    const formData = new FormData();
    const request: Object = new Object();
    formData.append('file', this.fileFormGroup.get('fileSource').value);

    this.store.addObject(formData, this.bucket.id);
  }

  @action.bound onFetchObjects(bucketId: string) {
    this.objectStore.getObjects(bucketId); 
  }

  @action.bound onDeleteObject(bucketId: string) {
    this.objectStore.deleteObject(bucketId, this.object.name);
    this.modalReference.close();
  }

  onSelectedObject(object: Object) {
    this.object = object; 
    this.selected = object;
    
  }

  openConfirmModal(content) {
    this.modalReference = this.modalService.open(content);
    this.modalReference.componentInstance;
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ngOnInit(): void {
   this.onFetchObjects(this.bucket.id);
  }  
}
