import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup } from '@angular/forms';

import { Location } from './modules/models/location.model';
import { Bucket } from './modules/models/bucket.model';
import { BucketStore } from './stores/bucket-store.store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'storage-app';

  locations: Location[];
  _buckets: Bucket[] = [];
  subscription: Subscription;
  bucketForm: FormGroup;

  constructor(public store: BucketStore){}

  ngOnInit() {
    this._buckets = this.store.buckets;
  }
}
