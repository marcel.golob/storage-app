import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MobxAngularModule } from 'mobx-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LocationsComponent } from './modules/components/locations/locations.component';
import { LocationItemComponent } from './modules/components/locations/location-item/location-item.component';
import { BucketsComponent } from './modules/components/buckets/buckets.component';
import { ObjectsComponent } from './modules/components/objects/objects.component';
import { BucketDetailsComponent } from './modules/components/buckets/bucket-details/bucket-details.component';
import { BucketAddComponent } from './modules/components/buckets/bucket-add/bucket-add.component';

import { BucketStore } from './stores/bucket-store.store';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ObjectStore } from './stores/object-store.store';
import { HeaderComponent } from './modules/components/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    LocationItemComponent,
    LocationsComponent,
    BucketsComponent,
    ObjectsComponent,
    BucketDetailsComponent,
    BucketAddComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MobxAngularModule,
    NgbModule,
  ],
  providers: [BucketStore, ObjectStore],
  bootstrap: [AppComponent]
})
export class AppModule { }
